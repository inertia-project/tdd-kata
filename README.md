### TDD Kata

This project contains collections of all TDD practical exercises.

* Kata 1 - [Simple Calculator](kata-1-simple-calculator-solution/src/test/java/com/inertia/tdd/kata/calculator/SimpleCalculatorTest.java) from this [link](http://osherove.com/tdd-kata-1/)