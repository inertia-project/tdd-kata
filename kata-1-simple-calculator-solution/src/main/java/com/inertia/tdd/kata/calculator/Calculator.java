package com.inertia.tdd.kata.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vinelovetets on 3/26/17.
 */
public class Calculator {

    private static final String DEFAULT_DELIMITERS = "[,\\n]";
    private static final String CUSTOM_DELIMITER_HINT = "//";

    public int add(String stringNumber) {
        int sum = 0;

        if (!isEmptyString(stringNumber)) {
            String addends = stringNumber;
            String customDelimiter = "";
            if (stringNumber.startsWith(CUSTOM_DELIMITER_HINT)) {
                String[] stringNumberMetadata = stringNumber.replaceAll("//", "").split("\\n", 2);
                customDelimiter = stringNumberMetadata[0];
                addends = stringNumberMetadata[1];
            }


            String allDelimiterRegex = customDelimiter.length() > 0 ? addCustomDelimiters(customDelimiter) : DEFAULT_DELIMITERS;
            String[] numbers = addends.split(allDelimiterRegex);

            StringBuilder negativeNumbers = new StringBuilder();
            for (String number : numbers) {

                int parseInteger = Integer.parseInt(number);
                if (parseInteger < 0) {
                    negativeNumbers.append(number).append(",");
                } else {
                    if (parseInteger <= 1000)
                        sum += parseInteger;
                }
            }

            if (negativeNumbers.length() > 0) {
                throw new NegativeNumberException(NegativeNumberException.DEFAULT_ERROR_MESSAGE + ":[" + negativeNumbers.deleteCharAt(negativeNumbers.length() - 1).toString() + "]");
            }

        }

        return sum;
    }

    private String addCustomDelimiters(String customDelimiter) {
        String allDelimiter = DEFAULT_DELIMITERS;

        String processedCustomDelimiter = "";
        if (isSingleDelimiter(customDelimiter)) {

            processedCustomDelimiter += "|" + enclosedDelimiter(customDelimiter);

        } else {

            processedCustomDelimiter += extractMultipleCustomDelimiters(customDelimiter);
        }

        allDelimiter += processedCustomDelimiter;

        return allDelimiter;
    }

    private boolean isSingleDelimiter(String customDelimiter) {
        return customDelimiter.length() == 1;
    }


    private String extractMultipleCustomDelimiters(String customDelimiter) {
        List<String> rawDelimiters = parseArrayOfCustomDelimiter(customDelimiter);

        return buildArrayOfCustomDelimiterAsRegex(rawDelimiters);
    }

    private List<String> parseArrayOfCustomDelimiter(String customDelimiter) {
        String multipleDelimiter = "(\\[)(.+?)(\\])";
        Pattern multipleDelimiterPattern = Pattern.compile(multipleDelimiter);
        Matcher matcher = multipleDelimiterPattern.matcher(customDelimiter);

        List<String> rawDelimiters = new ArrayList<>();
        while (matcher.find()) {
            rawDelimiters.add(matcher.group(2));
        }
        return rawDelimiters;
    }


    private String buildArrayOfCustomDelimiterAsRegex(List<String> rawDelimiters) {
        StringBuilder allDelimiter = new StringBuilder();
        for (String rawDelimiter : rawDelimiters) {
            allDelimiter.append("|").append(enclosedDelimiter(rawDelimiter));
        }
        return allDelimiter.toString();
    }

    private boolean isEmptyString(String s) {
        return s == null || s.isEmpty();
    }

    private String enclosedDelimiter(String delimiter) {
        return "(" + Pattern.quote(delimiter) + ")";
    }
}
