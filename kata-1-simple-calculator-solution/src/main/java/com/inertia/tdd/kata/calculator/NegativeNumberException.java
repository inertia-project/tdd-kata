package com.inertia.tdd.kata.calculator;

/**
 * Created by vinelovetets on 3/26/17.
 */
public class NegativeNumberException extends  RuntimeException{
    public static final String DEFAULT_ERROR_MESSAGE = "negatives not allowed";
    public NegativeNumberException() {
    }

    public NegativeNumberException(String message) {
        super(message);
    }

    public NegativeNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public NegativeNumberException(Throwable cause) {
        super(cause);
    }

    public NegativeNumberException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
