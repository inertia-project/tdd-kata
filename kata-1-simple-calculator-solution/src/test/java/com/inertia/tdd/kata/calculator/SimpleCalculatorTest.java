package com.inertia.tdd.kata.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by vinelovetets on 3/26/17.
 * Exercise from http://osherove.com/tdd-kata-1/
 */
@RunWith(JUnit4.class)
public class SimpleCalculatorTest {
    private Calculator calculator = new Calculator();

    @Test
    public void sumNull() {
        assertEquals(0, calculator.add(null));
    }

    @Test
    public void sumEmptyString() {
        assertEquals(0, calculator.add(""));
    }

    @Test
    public void sumSingleNumber() {
        for (int number = 0; number < 10; number++)
            assertEquals(number, calculator.add(String.valueOf(number)));
    }

    @Test
    public void sumTwoNumber() {
        assertEquals(300, calculator.add("200,100"));
    }

    @Test
    public void sumMultipleNumber() {
        assertEquals(300, calculator.add("50,100,70,80"));
    }

    @Test
    public void sumWithNewline() {
        assertEquals(300, calculator.add("50\n100\n70,80"));
    }

    @Test
    public void sumWithCustomizableDelimeter() {
        assertEquals(300, calculator.add("//;\n50;100;70,80"));
    }

    @Test
    public void sumWithNegativeNumbers() {
        boolean exceptionHappens = false;
        try {
            calculator.add("50,-100,70,-80");
        } catch (NegativeNumberException e) {
            assertEquals("negatives not allowed:[-100,-80]", e.getMessage());
            exceptionHappens = true;
        }

        assertTrue(exceptionHappens);
    }

    @Test
    public void sumWithAboveThousandAddends() {

        assertEquals(300, calculator.add("//;\n1001;50;100;70,80\n2000"));
    }

    @Test
    public void sum_WithCustomDelimiter_MoreThanOneCharacter() {
        assertEquals(6, calculator.add("//[***]\n1***2***3"));
    }

    @Test
    public void sum_WithCustomDelimeter_Multiple() {
        assertEquals(6, calculator.add("//[*][%]\n1*2%3"));
    }
}
